const playPauseButton = document.getElementById("play-pause-button");
const stopButton = document.getElementById("stop-button");
const recordButton = document.getElementById("record-button");
const clearButton = document.getElementById("clear-button");
const recordList = document.getElementById("list");

const hoursField = document.getElementById("hours");
const minutesField = document.getElementById("minutes");
const secondsField = document.getElementById("seconds");
const milisecondsField = document.getElementById("miliseconds");

let hours = 0,
	hoursDisplay = 0;
let minutes = 0,
	minutesDisplay = 0;
let seconds = 0,
	secondsDisplay = 0;
let miliseconds = 0,
	milisecondsDisplay = 0;

let interval;

window.addEventListener('load', () => {
    formatAllDigit();
})

playPauseButton.addEventListener("click", () => {
	if (playPauseButton.classList.contains("pause")) {
		playPauseButton.classList.remove("pause");
		playPauseButton.innerHTML = `<ion-icon name="play-outline"></ion-icon>`;
		pause();
	} else {
		playPauseButton.classList.add("pause");
		playPauseButton.innerHTML = `<ion-icon name="pause-outline"></ion-icon>`;
		play();
	}
});

stopButton.addEventListener("click", () => {
	stop();
});

recordButton.addEventListener("click", () => {
	record();
});

clearButton.addEventListener("click", () => {
	clearList();
});

const play = () => {
	interval = setInterval(timer, 10);
};

const pause = () => {
	clearInterval(interval);
};

const stop = () => {
	if (playPauseButton.classList.contains("pause")) {
		playPauseButton.classList.remove("pause");
		playPauseButton.innerHTML = `<ion-icon name="play-outline"></ion-icon>`;
	}

	pause();

	hours = 0;
	minutes = 0;
	seconds = 0;
	miliseconds = 0;

	formatAllDigit();

	display("00", "00", "00", "00");
};

const record = () => {
	let item = document.createElement("li");
	item.textContent = `${hoursDisplay} : ${minutesDisplay} : ${secondsDisplay} : ${milisecondsDisplay}`;
	recordList.appendChild(item);
};

const clearList = () => {
	recordList.innerHTML = "";
};

const timer = () => {
	formatAllDigit();

	++miliseconds;

	if (miliseconds === 100) {
		miliseconds = 0;
		++seconds;
	}

	if (seconds === 60) {
		seconds = 0;
		++minutes;
	}

	if (minutes === 60) {
		minutes = 0;
		++hours;
	}

	display(hoursDisplay, minutesDisplay, secondsDisplay, milisecondsDisplay);
};

const digitFormatter = (number) => {
	if (number < 10) {
		number = `0${number}`;
	}
	return number;
};

const formatAllDigit = () => {
	hoursDisplay = digitFormatter(hours);
	minutesDisplay = digitFormatter(minutes);
	secondsDisplay = digitFormatter(seconds);
	milisecondsDisplay = digitFormatter(miliseconds);
};

const display = (hours, minutes, seconds, miliseconds) => {
	hoursField.innerHTML = hours;
	minutesField.innerHTML = minutes;
	secondsField.innerHTML = seconds;
	milisecondsField.innerHTML = miliseconds;
};
